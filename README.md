# Descrition
Used to build all the FreeingReturn projects

* should be placed at the same level of all the desired projects

# Release notes:
## 0.2.0:
* moved all ROE's to *roe.global*

## 0.2.1:
* added failure string to add's
    * affected projects: all
    * XSD's need reload

